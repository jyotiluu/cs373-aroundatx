echo "Deploying Backend..."
aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 116011505375.dkr.ecr.us-east-2.amazonaws.com
docker build -t aroundatx-backend .
docker tag aroundatx-backend:latest 116011505375.dkr.ecr.us-east-2.amazonaws.com/aroundatx-backend:latest
docker push 116011505375.dkr.ecr.us-east-2.amazonaws.com/aroundatx-backend:latest
cd aws_deploy
eb deploy