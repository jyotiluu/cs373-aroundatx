import React from "react";
import './App.css';
import About from "./Pages/About/About";
import Hotels from "./Pages/Hotels/Hotels";
import Incidents from "./Pages/Incidents/Incidents";
import Restaurants from "./Pages/Restaurants/Restaurants";
import Search from "./Pages/Search/Search"
import Splash from "./Pages/Splash/Splash";
import NavBar from "./Components/Navbar/Navbar";
import HotelInstance from "./Pages/Hotels/HotelInstance";
import RestaurantInstance from "./Pages/Restaurants/RestaurantInstance";
import IncidentInstance from "./Pages/Incidents/IncidentInstance";
import Visualizations from "./Pages/Visualizations/Visualizations";
import ProviderVisualizations from "./Pages/Visualizations/ProviderVisualizations";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

function App() {
    return ( 
        <div className="App">
            <Router> 
                <NavBar/>
                <Switch>
                    <Route exact path='/'> <Splash/> </Route>
                    <Route exact path='/hotels'> <Hotels/> </Route>
                    <Route exact path='/incidents'> <Incidents/> </Route>
                    <Route exact path='/restaurants'> <Restaurants/> </Route>
                    <Route exact path='/about'> <About/> </Route>
                    <Route exact path='/visualizations'> <Visualizations/> </Route>
                    <Route exact path='/provider-visualizations'> <ProviderVisualizations/> </Route>
                    <Route exact path='/search'> <Search/> </Route>
                    <Route path='/hotels/:id'> <HotelInstance/> </Route>
                    <Route path='/restaurants/:id'> <RestaurantInstance /> </Route>
                    <Route path='/incidents/:id'> <IncidentInstance /> </Route>
                </Switch>
            </Router>
        </div>
    );
}

export default App;