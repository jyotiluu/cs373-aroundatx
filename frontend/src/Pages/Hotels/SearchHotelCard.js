import React, {Component} from "react";
import { Card, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';
import './Hotels.css';

class SearchHotelCard extends Component {

	getHighlightedText(text, highlight) {
		let textLowerCases = text.toLowerCase();
		let hightLowerCases = highlight.toLowerCase();
		let matchedIndex = textLowerCases.indexOf(hightLowerCases);

		if (matchedIndex === -1) {
			return text
		}

		return (
			<React.Fragment>
				{text.substring(0, matchedIndex)}
				<span id="search-span">{text.substring(matchedIndex, matchedIndex + highlight.length)}</span>
				{text.substring(matchedIndex + highlight.length)}
			</React.Fragment>
		);
	}

	render() {
		let hit = this.props.hit;

		return (
			<Col>
				<Card className="mb-2" style={{height:"300px", width:"300px"}}>
					<Card.Img 
						variant="top" 
						src={hit.image} 
						alt="profile-image" 
						style={{height:"40%", width:"auto"}}
					/>
					<Card.Body>
						<h6>
						<Link to={`/hotels/${hit.hotel_id}`}>
							<div>{this.getHighlightedText(hit.name, this.props.query)}</div>
						</Link>
						</h6>
						<Card.Text>
							Latitude: {hit.latitude} <br />
							Longitude: {hit.longitude} <br />
							Star Rating: {this.getHighlightedText(String(hit.star_rating), this.props.query)}/5 <br />
							Guest Rating: {this.getHighlightedText(String(hit.guest_rating), this.props.query)}/10 <br />
							Distance to City Center: {this.getHighlightedText(String(hit.distance_to_city_center), this.props.query)} miles <br />
							Distance to Airport: {this.getHighlightedText(String(hit.distance_to_airport), this.props.query)} miles <br />
						</Card.Text>
					</Card.Body>
        		</Card>
			</Col>
		)
	}
}

export default SearchHotelCard;