import React, { useState, useEffect } from 'react';
import axios from 'axios';
import {Row, Container} from 'react-bootstrap';
import algoliasearch from 'algoliasearch/lite';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Pagination from '@material-ui/lab/Pagination';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Input from '@material-ui/core/Input';
import HotelCard from './HotelCard.js';
import SearchHotelCard from './SearchHotelCard.js';
import algolialogo from '../Search/algolialogo.png';
import Image from "react-bootstrap/Image";
import bed from "../Assets/bed.png"
import 'bootstrap/dist/css/bootstrap.min.css';
import './Hotels.css';

const searchClient = algoliasearch('E5EGQ12D8H', '6e5cf15ccc243187e411d3049cfd95da');
const index = searchClient.initIndex('hotel_Aroundatx');

const useStyles = makeStyles((theme) => ({
    root: {
        padding: '2px 4px',
        display: 'block',
        margin: '0 auto',
        width: 400
    },
    input: {
        display: 'flex',
        margin: '0 auto',
        flexWrap: 'wrap'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing(2),
    },
}));

export default function Hotels() {

    const classes = useStyles();
    const perPage = 8;
    const [instanceCount, setInstanceCount] = useState(0);
    const [apiData, setAPIData] = useState(null);
    const [searchedData, setSearchedData] = useState(null);
    const [displayCards, setDisplayCards] = useState(null);
    const [totalPages, setTotalPages] = useState(0);
    const [currentPage, setCurrentPage] = useState(0);
    const [isUseAPIData, setIsUseAPIData] = useState(true);
    const [sortingAttribute, setSortingAttribute] = useState(null);
    const [sortingOrder, setSortingOrder] = useState(null);
    const [filterArgs, setFilterArgs] = useState({
        "name": ["A", "Z", getRangeFromLetters("A", "Z", 1)],
        "star_rating": [0, 5, getRangeFrom(0, 5, 1)],
        "guest_rating": [0, 10, getRangeFrom(0, 10, 1)],
        "distance_to_airport": [0, 20, getRangeFrom(0, 20, 1)],
        "distance_to_city_center": [0, 20, getRangeFrom(0, 20, 1)]
    });
    const [open, setOpen] = useState(false);
    const [searchQuery, setSearchQuery] = useState("");

    useEffect(() => {
        if (apiData == null) {
            fetchHotelDataFromUrl('/api/hotels');
        } else if (isUseAPIData) {
            sliceData(apiData);
        } else {
            sliceData(searchedData);
        }
    });

    // Slices the data, converts it to a specific form for rendering
    function sliceData(data) {
        if (isUseAPIData) {
            // it is using the API data for now
            let start = (currentPage - 1) * perPage
            let slice = data.slice(start, Math.min(start + perPage, data.length));
            let postData = slice.map(pd => 
                <React.Fragment>
                    <HotelCard data={pd}/>
                </React.Fragment>
            );
            setDisplayCards(postData);
            setInstanceCount(data.length);
        } else {
            // using data searched from algolia
            let start = (currentPage - 1) * perPage;
            let slice = data.slice(start, Math.min(start + perPage, data.length));
            let postData = slice.map(pd => 
                <React.Fragment>
                    <SearchHotelCard hit={pd} query={searchQuery}/>
                </React.Fragment>
            );
            setDisplayCards(postData);
            setInstanceCount(data.length);
        }
    }

    // Handles when the page in pagination changes
    function pageChangeHandler(event, number) {
        setCurrentPage(number);
    }

    // Handles the search
    function searchChangeHandler(event) {
        let query = event.target.value;
        if (query != null && query.length > 0) {
            index.search(query).then(result => {
                let data = result.hits;
                setSearchQuery(query);
                setSearchedData(data);
                setIsUseAPIData(false);
                setTotalPages(Math.ceil(data.length / perPage));
                setCurrentPage(1);
            })
        } else {
            setIsUseAPIData(true);
            setTotalPages(Math.ceil(apiData.length / perPage));
            setCurrentPage(1);
        }
    }

    // Handler for the values changles in sort selection
    function sortAttributeChangeHandler(event) {
        let value = event.target.value;
        setSortingAttribute(value);

        if (isUseAPIData) {
            let url = `/api/hotels?sort_by=${value}&sort_direction=${sortingOrder}`;
            fetchHotelDataFromUrl(url);
        } else {
            // Uses the searched result
            let data = [...searchedData].sort((a, b) => {
                let compare = a[value] > b[value] ? 1 : -1;
                return sortingOrder === "desc" ? -compare : compare;
            });
            setSearchedData(data);
        }
    }

    // fetch the hotel from the api with different arguments
    function fetchHotelDataFromUrl(url) {
        axios.get(url).then(response => {
            let data = response.data["hotels"];
            setAPIData(data);
            setTotalPages(Math.ceil(data.length / perPage));
            setCurrentPage(1);
        });
    }

    // Hanlder when the sorting order changes
    function sortingOrderChangeHandler(event) {
        let value = event.target.value;
        setSortingOrder(value);

        if (isUseAPIData && sortingAttribute != null) {
            let url = `/api/hotels?sort_by=${sortingAttribute}&sort_direction=${value}`;
            fetchHotelDataFromUrl(url);
        } else if (sortingAttribute != null) {
            // Uses the searched result
            let data = [...searchedData].sort((a, b) => {
                let compare = a[sortingAttribute] > b[sortingAttribute] ? 1 : -1;
                return value === 'desc' ? -compare : compare;
            });
            setSearchedData(data);
        }
    }

    // Called when click the filter button
    const handleClickOpen = () => {
        setOpen(true);
    };

    // Called when close button is clicked
    const handleClose = () => {
        setOpen(false);
    };

    // Called when the OK button is clicked
    const handleClickOk = () => {
        let args = [];
        for (const criteria in filterArgs) {
            let from = filterArgs[criteria][0];
            let to  = filterArgs[criteria][1];
            if (!(from === 0 && to === 0 ) && from <= to) {
                let query = `${criteria}_from=${from}&${criteria}_to=${to}`;
                args.push(query);
            }
        }
        let url = "api/hotels?" + args.join("&");
        fetchHotelDataFromUrl(url);
        setOpen(false);
    };

    function getRangeFrom(start, end, step) {
        var ans = [];
        for (let i = start; i <= end; i += step) {
            ans.push(<MenuItem value={i}>{i}</MenuItem>);
        }
        return ans;
    }

    function getRangeFromLetters(start, end, step) {
        var ans = [];
        for (let i = start.charCodeAt(0); i <= end.charCodeAt(0); i += step) {
            ans.push(<MenuItem value={String.fromCharCode(i)}>{String.fromCharCode(i)}</MenuItem>);
        }
        return ans;
    }

    function createFilterSelection(criteria, name) {
        const handleFromValueChange = (event) => {
            let row = filterArgs[event.target.name];
            row[0] = event.target.value;
            setFilterArgs({ ...filterArgs, [event.target.name]: row });
        };

        const handleToValueChange = (event) => {
            let row = filterArgs[event.target.name];
            row[1] = event.target.value;
            setFilterArgs({ ...filterArgs, [event.target.name]: row });
        }

        return (
            <React.Fragment>
                <form className={classes.container}>
                <FormControl className={classes.formControl}>
                    <InputLabel htmlFor="demo-dialog-native">{name}</InputLabel>
                </FormControl>
                <FormControl className={classes.formControl}>
                    <InputLabel >From</InputLabel>
                    <Select
                    value={filterArgs[criteria][0]}
                    name={criteria}
                    onChange={handleFromValueChange}
                    input={<Input />}
                    >
                    <MenuItem value="">
                        <em>None</em>
                    </MenuItem>
                    {filterArgs[criteria][2]}
                </Select>
            </FormControl>
            <FormControl className={classes.formControl}>
                 <InputLabel>To</InputLabel>
                    <Select
                        value={filterArgs[criteria][1]}
                        name={criteria}
                        onChange={handleToValueChange}
                        input={<Input />}
                    >
                        <MenuItem value="">
                            <em>None</em>
                        </MenuItem>
                        {filterArgs[criteria][2]}
                    </Select>
            </FormControl>
            </form>
            </React.Fragment>
        );
    }

    return (
        <div className="body" style={{float:"center"}}>
            <div className="table-container">
                <div class="grid" style={{alignContent:'center'}}>
                    <div class="row align-items-center">
                        <div class="col-xl-4">
                            <img src={bed} style={{ width: "40%", height: "40%" }} alt="Unable to load"></img>
                        </div>
                        <div class="col-xl-4">
                            <h2 className="page-title">Hotels</h2>
                            <p style={{textAlign:"center"}}>Explore hotel stays in Austin.</p>
                            <Paper component="form" className={classes.root} >
                                <InputBase
                                    className={classes.input}
                                    placeholder="search hotels"
                                    inputProps={{ 'aria-label': 'search hotels' }}
                                    onChange={searchChangeHandler}
                                />
                            </Paper>
                            <FormControl className={classes.formControl}>
                                <InputLabel>Sort by</InputLabel>
                                <Select
                                    value={sortingAttribute}
                                    onChange={sortAttributeChangeHandler}>
                                    <MenuItem value={"name"}>Name</MenuItem>
                                    <MenuItem value={"star_rating"}>Star Rating</MenuItem>
                                    <MenuItem value={"guest_rating"}>Guest Rating</MenuItem>
                                    <MenuItem value={"distance_to_airport"}>Distance to Airport</MenuItem>
                                    <MenuItem value={"distance_to_city_center"}>Distance to City Center</MenuItem>
                                </Select>
                            </FormControl>

                            <FormControl className={classes.formControl}>
                                <InputLabel>Sort Order</InputLabel>
                                <Select
                                    value={sortingOrder}
                                    onChange={sortingOrderChangeHandler}>
                                    <MenuItem value={"asc"}>Ascending</MenuItem>
                                    <MenuItem value={"desc"}>Descending</MenuItem>
                                </Select>
                            </FormControl>

                            <FormControl className={classes.formControl}>
                                <Button onClick={handleClickOpen}>Filter</Button>
                                <Dialog disableBackdropClick disableEscapeKeyDown open={open} onClose={handleClose}>
                                    <DialogTitle>Select Filter Options</DialogTitle>
                                    <DialogContent>
                                    {createFilterSelection("name", "Name")}
                                    {createFilterSelection("star_rating", "Star Rating")}
                                    {createFilterSelection("guest_rating", "Guest Rating")}
                                    {createFilterSelection("distance_to_airport", "Distance to Airport")}
                                    {createFilterSelection("distance_to_city_center", "Distance to City Center")}
                                    </DialogContent>
                                        <DialogActions>
                                            <Button onClick={handleClose} color="primary">
                                                Cancel
                                            </Button>
                                            <Button onClick={handleClickOk} color="primary">
                                                Ok
                                            </Button>
                                        </DialogActions>
                                </Dialog>
                            </FormControl>
                        </div>
                        <div class="col-xl-4">
                            <img src={bed} style={{ width: "40%", height: "40%" }} alt="Unable to load"></img>
                        </div>
                    </div>
                </div>
                <Container fluid>
                    <Row className="hotel-row" md={5} lg={5}>
                        {displayCards}
                    </Row>
                    <Pagination 
                        page={currentPage} 
                        count={totalPages} 
                        showFirstButton 
                        showLastButton 
                        onChange={pageChangeHandler} 
                    />
                </Container>
                <div className="instance-count">
       			    <div>Total Number of Instances: {instanceCount}</div>
      		    </div>
            </div>
            <div className="algolialogo">
       			<div>Search Powered by &nbsp;</div>
    			<div>
          			<Image
            			src={algolialogo}
            			height="5%"
            			width="5%"
                    />
        		</div>
      		</div>
      		<br />
        </div>
    )
}
