import React, { Component } from 'react'
import Leaflet from '../../Components/Leaflet'
import axios from 'axios'
import dish from "../Assets/dish.png"
import alert from "../Assets/alert.png"
import Card from "react-bootstrap/Card"
import './HotelInstance.css'
import 'leaflet/dist/leaflet.css'

const twoStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/2_stars.svg/1280px-2_stars.svg.png'
const twoHalfStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/2.5_stars.svg/1280px-2.5_stars.svg.png'
const threeStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/3_stars.svg/1024px-3_stars.svg.png'
const threeHalfStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/3.5_stars.svg/1280px-3.5_stars.svg.png'
const fourStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/4_stars.svg/1200px-4_stars.svg.png'
const fourHalfStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/4.5_stars.svg/1280px-4.5_stars.svg.png'

class HotelInstance extends Component {

    constructor(props) {
        super(props);
        this.state = {
            hotelData: null,
            restaurantData: null,
            incidentData: null
        }
    }

    async componentDidMount() {
        const regex = /\d+/g;
        const id = window.location.pathname.match(regex);
        const hotelRes = await axios.get(`/api/hotels/id=${id[0]}`);
        const restaurantRes = await axios.get(`/api/restaurants/zipcode=${hotelRes.data['zipcode']}`);
        const incidentRes = await axios.get(`/api/incidents/zipcode=${hotelRes.data['zipcode']}`)

        this.setState({
            hotelData: hotelRes.data,
            restaurantData: restaurantRes.data["restaurants"],
            incidentData: incidentRes.data["incidents"]
        })
    }

    showStars(rating) {
        if (rating === 2.0)
            return twoStars;
        else if (rating === 2.5)
            return twoHalfStars;
        else if (rating === 3.0)
            return threeStars;
        else if (rating === 3.5)
            return threeHalfStars;
        else if (rating === 4.0)
            return fourStars;
        else 
            return fourHalfStars;
    }

    displayRestaurant() {
        if (this.state.restaurantData == null) {
            return null;
        } else if (this.state.restaurantData.length === 0) {
            return (<p>No restaurants found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.restaurantData.length; i++) {
            let restaurant = this.state.restaurantData[i];
            item.push(
                <React.Fragment>
                    <li>
                        <a href={"/restaurants/" + restaurant["restaurant_id"]}>{restaurant["name"]}</a>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    displayIncident() {
        if (this.state.incidentData == null) {
            return null;
        } else if (this.state.incidentData.length === 0) {
            return (<p>No incidents found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.incidentData.length; i++) {
            let incident = this.state.incidentData[i];
            var stop = incident['address'].indexOf(',');
            var street = this.capitalizeFirstLetters(incident['address'].substring(0, stop));

            item.push(
                <React.Fragment>
                    <li>
                        <a href={"/incidents/" + incident["incident_id"]}>{incident["name"]}</a>
                        <a>: {street}</a>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    capitalizeFirstLetters(text) {
        return text.toLowerCase().replace(/(^[a-z]| [a-z])/g, function (match, letter) {    
            return letter.toUpperCase();
        });
    }

    render() {
        let hotelData = this.state.hotelData;
        let restaurantData = this.state.restaurantData;
        let incidentData = this.state.incidentData;

        return (hotelData != null && restaurantData != null && incidentData != null) ? (
            <React.Fragment>
                <div style={{ margin: 'auto', marginTop: '10vh', width: '90%' }}>
                    <h1 className="text-center">{hotelData.name}</h1>
                    <img src={this.showStars(hotelData['star_rating'])} style={{ width: "10%", height: "10%" }} alt="Unable to load"></img>
                    <br></br><br></br>
                    <h4 className="text-center">Scroll down to view more information!</h4>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
                    <Leaflet latitude={hotelData["latitude"].toFixed(2)} longitude={hotelData["longitude"].toFixed(2)} ></Leaflet>
                    <div className="row">
                        <div className="col-sm">
                            <Card.Body>
                                <Card.Title ><h2>Location</h2></Card.Title>
                                <Card.Text style={{ fontSize: "25px" }}>
                                    {hotelData.street_address}, {hotelData.city}, {hotelData.state} {hotelData.zipcode}<br></br>
                                    {"Latitude: " + hotelData["latitude"]}<br></br>
                                    {"Longitude: " + hotelData["longitude"]}<br></br><br></br>
                                    <div class="grid" style={{alignContent:'center'}}>
                                        <div class="row align-items-center">
                                            <div class="col-xl-6">
                                                <Card.Img src={hotelData.image} style={{ width: "80%", height: "80%" }} />
                                            </div>
                                            <div class="col-xl-6">
                                                <br></br><br></br>
                                                <Card.Title><h2><br></br>Details</h2></Card.Title>
                                                {"Stars: " + hotelData["star_rating"]}<br></br>
                                                {"Average Guest Rating: " + hotelData["guest_rating"]}<br></br>
                                                {"Distance to City Center: " + hotelData["distance_to_city_center"]} miles<br></br>
                                                {"Distance to Airport: " + hotelData["distance_to_airport"]} miles<br></br>
                                                {"Neighborhood: " + hotelData["neighborhood"]}<br></br>
                                                {"Price per Night: $" + hotelData["price"]}<br></br>
                                                {"Rooms Available: " + hotelData["rooms_left"]}<br></br><br></br><br></br><br></br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid" style={{alignContent:'center'}}>
                                        <div class="row align-items-start">
                                            <div class="col-xl-6">
                                                <Card.Img variant="top" className="center" src={dish} style={{ width: "15%" }} />
                                                <Card.Title><h2><br></br>Nearby Restaurants</h2></Card.Title>
                                                <div style={{ listStyle: "none" }}>{this.displayRestaurant()}</div><br></br><br></br><br></br>
                                            </div>
                                            <div class="col-xl-6">
                                                <Card.Img variant="top" className="center" src={alert} style={{ width: "15%" }} />
                                                <Card.Title><h2><br></br>Nearby Incidents</h2></Card.Title>
                                                <div style={{ listStyle: "none" }}>{this.displayIncident()}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        ) : (
            <div>
                <h2><br></br><br></br><br></br>Loading...</h2>
            </div>
        );
    }
}

export default HotelInstance;