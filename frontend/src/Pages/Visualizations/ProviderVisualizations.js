import React from "react";
import ArtistsChart from "./Spotifynder/ArtistsChart";
import SongsChart from "./Spotifynder/SongsChart";
import AlbumsChart from "./Spotifynder/AlbumsChart";

export default function ProviderVisualizations() {
    return (
        <React.Fragment>
            <div style={{margin: 'auto', marginTop: '10vh', width: '90%'}}>
                <h1 className="text-center">Provider Visualizations</h1>
            </div>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <ArtistsChart></ArtistsChart>
            </div>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <SongsChart></SongsChart>
            </div>
            <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                <AlbumsChart></AlbumsChart>
            </div>
        </React.Fragment>
    )
}