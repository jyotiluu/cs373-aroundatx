import React, {Component} from "react";
import axios from "axios";
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
} from "recharts";

class ArtistsChart extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            data: null
        }
    }

    async componentDidMount() {
        const url = 'https://spotifynder.com/api/artists?sort=popularity&direction=1&strGenre=Hip-Hop';
        const result = await axios.get(url);

        this.setState({
            data : result.data.message
        })
    }

    getValues() {
        var i = 0;
        var count = 0;
        var values = [];

        while (count < 10) {
            var artistName = this.state.data[i]["name"];
            if (artistName === 'XXXTENTACION')
                artistName = 'X';
            var popularity = this.state.data[i]["popularity"]
            values.push({"name" : artistName, "popularity" : popularity})
            i++;
            count++; 
        }
        
        return values;
    }

    render() {
        let data = this.state.data;

        return (data != null) ? (
            <div style={{margin: 'auto', marginTop: '5vh'}} >
                <h3 className="text-center"> Popularity of Top 10 Hip-Hop Arists </h3>
                <p className="text-center"> Hover to view exact values</p>
                <p className="text-center"> (x - Artist Name, y - Popularity)</p>
                <BarChart
                    width={1000}
                    height={500}
                    data={this.getValues()}
                    margin={{top: 5, right: 30, left: 20, bottom: 5}}
                    barSize={20}>
                    <XAxis dataKey="name" scale="point" padding={{left: 100, right: 10 }}/>
                    <YAxis type="number" domain={[90, 100]}/>
                    <Tooltip/>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <Bar dataKey="popularity" fill="#1DB954" background={{ fill: "#eee" }}/>
                </BarChart>
            </div>
        ) : (
            <div>
                <h2><br></br><br></br><br></br>Loading...</h2>
            </div>
        );
    }
}

export default ArtistsChart;
