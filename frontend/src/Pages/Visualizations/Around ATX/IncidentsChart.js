import React, { useEffect, useState } from "react";
import axios from "axios";
import { Pie, PieChart, Tooltip } from "recharts";

export default function IncidentsChart() {

    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('/api/incidents').then(response => {
                setData(response.data.incidents)
            }).catch((error) => {
                setData([])
            });
        }
        fetchData();
    }, []);

    function getValues() {
        var values = [];
        var dict = {};

        for (var i = 0; i < data.length; i++) {
            var method = data[i]["method_recieved"];
            if (method in dict) {
                dict[method]++;
            }
            else {
                dict[method] = 0;
            }
        }

        for (var method in dict) {
            var randomColor = "#" + Math.floor(Math.random() * 16777215).toString(16);
            values.push({"name": method, "value": dict[method], "fill": randomColor})
        }

        return values;
    }

    return (
        <div style={{ margin: 'auto', marginTop: '5vh'}} >
            <h3 className="text-center"> Frequency of Methods Received for Incidents </h3>
            <p className="text-center"> Hover to view values </p>
            <PieChart 
                width={1000} 
                height={800} 
                margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                <Pie style={{paddingLeft:'50px'}}
                    dataKey="value"
                    isAnimationActive={true}
                    data={getValues()}
                    cx={485}
                    cy={350}
                    outerRadius={300}
                    fill="#fff"
                    label
                />
                <Tooltip />
            </PieChart>
        </div>
    );
}
