import React, {useEffect, useState} from "react";
import axios from "axios";
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
} from "recharts";

export default function RestaurantsChart() {

    const [data, setData] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('/api/restaurants').then(response => {
                setData(response.data.restaurants)
            }).catch((error) => {
                setData([])
            });
        }
        fetchData();
    }, []);

    function getValues() {
        var dict = {
            1.0 : 0,
            1.5 : 0,
            2.0 : 0,
            2.5 : 0,
            3.0 : 0,
            3.5 : 0,
            4.0 : 0,
            4.5 : 0,
            5.0 : 0,
        };

        for (var i = 0; i < data.length; i++) {
            var val = data[i]["rating"];
            dict[val]++;
        }

        var values = [
            {
                name: "1.0",
                value: dict[1.0],
            },
            {
                name: "1.5",
                value: dict[1.5],
            },
            {
                name: "2.0",
                value: dict[2.0],
            },
            {
                name: "2.5",
                value: dict[2.5],
            },
            {
                name: "3.0",
                value: dict[3.0],
            },
            {
                name: "3.5",
                value: dict[3.5],
            },
            {
                name: "4.0",
                value: dict[4.0],
            },
            {
                name: "4.5",
                value: dict[4.5],
            },
            {
                name: "5.0",
                value: dict[5.0],
            },
        ];

        return values;
    }

    return (
        <div style={{margin: 'auto', marginTop: '5vh'}} >
            <h3 className="text-center"> Number of Restaurants by Rating </h3>
            <p className="text-center"> Hover to view exact values</p>
            <p className="text-center"> (x - Rating, y - Count)</p>
            <BarChart
                width={1000}
                height={500}
                data={getValues()}
                margin={{top: 5, right: 30, left: 20, bottom: 5}}
                barSize={20}>
                <XAxis dataKey="name" scale="point" padding={{left: 100, right: 10 }}/>
                <YAxis type="number" domain={[0, 200]}/>
                <Tooltip/>
                <CartesianGrid strokeDasharray="3 3"/>
                <Bar dataKey="value" fill="#bf5700" background={{ fill: "#eee" }}/>
            </BarChart>
        </div>
    );
}
