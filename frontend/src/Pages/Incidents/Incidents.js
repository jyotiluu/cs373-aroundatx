import React, {useEffect, useState} from 'react'
import axios from 'axios';
import MUIDataTable from "mui-datatables"; 
import Highlighter from 'react-highlight-words';
import alert from '../Assets/alert.png';
import 'bootstrap/dist/css/bootstrap.min.css';
import './Incidents.css'


function Incident() {
    const [data, setData] = useState([]);
    const [searchText, setSearchText] = useState("");

    useEffect(() => {
        const fetchData = async () => {
            await axios.get('/api/incidents').then(response => {
                setData(response.data.incidents)
            }).catch((error) => {
                setData([])
            });
        }
        fetchData();
    }, []);

    const incidentCustomBodyRender = (value, tableMeta, updateValue) => (
        <div>
            <Highlighter
                highlightClassName="highlight-class"
                searchWords={[searchText]}
                textToHighlight={value + ""}
            ></Highlighter>
        </div>
      );


    const columns = [
        {
            name: 'incident_id',
            label: 'Incident id',
            options: {
                filter: false,
                sort: false,
                display: "excluded",
            },
        },
        {
            name: 'name',
            label: 'Name',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(incident_name, filters) {
                        const show = 
                            (filters.indexOf('A-I') >= 0 && incident_name.charCodeAt(0) >= "A".charCodeAt(0) 
                                                         && incident_name.charCodeAt(0) <= "I".charCodeAt(0)) ||

                            (filters.indexOf('J-R') >= 0 && incident_name.charCodeAt(0) >= "J".charCodeAt(0) 
                                                         && incident_name.charCodeAt(0) <= "R".charCodeAt(0)) ||
                                                    
                            (filters.indexOf('S-Z') >= 0 && incident_name.charCodeAt(0) >= "S".charCodeAt(0) 
                                                         && incident_name.charCodeAt(0) <= "Z".charCodeAt(0));
                        return !show;

                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    incidentCustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            name: 'department',
            label: 'Department',
            options: {
                filter: false,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ['A-I', 'J-R', 'S-Z'],
                    logic(incident_name, filters) {
                        const show = 
                            (filters.indexOf('A-I') >= 0 && incident_name.charCodeAt(0) >= "A".charCodeAt(0) 
                                                         && incident_name.charCodeAt(0) <= "I".charCodeAt(0)) ||

                            (filters.indexOf('J-R') >= 0 && incident_name.charCodeAt(0) >= "J".charCodeAt(0) 
                                                         && incident_name.charCodeAt(0) <= "R".charCodeAt(0)) ||
                                                    
                            (filters.indexOf('S-Z') >= 0 && incident_name.charCodeAt(0) >= "S".charCodeAt(0) 
                                                         && incident_name.charCodeAt(0) <= "Z".charCodeAt(0));
                        return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    incidentCustomBodyRender(value, tableMeta, updateValue),
            },
    
        },
        {
            name: 'council_district',
            label: 'Council District',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: [1,2,3,4,5,6,7,8,9,10],
                    logic(council_district, filters) {
                        const show = 
                            (filters.indexOf(1) >= 0 && council_district === 1) ||
                            (filters.indexOf(2) >= 0 && council_district === 2) ||
                            (filters.indexOf(3) >= 0 && council_district === 3) ||
                            (filters.indexOf(4) >= 0 && council_district === 4) ||
                            (filters.indexOf(5) >= 0 && council_district === 5) ||
                            (filters.indexOf(6) >= 0 && council_district === 6) ||
                            (filters.indexOf(7) >= 0 && council_district === 7) ||
                            (filters.indexOf(8) >= 0 && council_district === 8) ||
                            (filters.indexOf(9) >= 0 && council_district === 9) ||
                            (filters.indexOf(10) >= 0 && council_district === 10);
                        return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    incidentCustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            name: 'method_recieved',
            label: 'Method Received',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ['Phone', 'E-Mail', 'Web', 'Spot331 Interface', 'Other' , 'Open311'],
                    logic(method_recieved, filters){
                        const show = 
                            (filters.indexOf('Phone') >= 0 && method_recieved === 'Phone') ||
                            (filters.indexOf('E-Mail') >= 0 && method_recieved === 'E-Mail') ||
                            (filters.indexOf('Web') >= 0 && method_recieved === 'Web') ||
                            (filters.indexOf('Spot331 Interface') >= 0 && method_recieved === 'Spot331 Interface') ||
                            (filters.indexOf('Open311') >= 0 && method_recieved === 'Open311') ||
                            (filters.indexOf('Other') >= 0 && method_recieved === 'Other');
                        return !show;

                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    incidentCustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            name: 'description',
            label: 'Description',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ['New', 'Open', 'Closed', 'Resolved','Work In Progress'],
                    logic(description, filters) {
                        const show = 
                            (filters.indexOf('New') >= 0 && description === 'New') ||
                            (filters.indexOf('Open') >= 0 && description === 'Open') ||                   
                            (filters.indexOf('Closed') >= 0 && description === 'Closed') ||
                            (filters.indexOf('Resolved') >= 0 && description === 'Resolved') ||
                            (filters.indexOf('Work In Progress') >= 0 && description === 'Work In Progress');
                        return !show;
                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    incidentCustomBodyRender(value, tableMeta, updateValue),
            },
        },
        {
            name: 'created',
            label: ' Date Created',
            options: {
                filter: false,
                sort: true,
            },
        },
        {
            name: 'county',
            label: 'County',
            options: {
                filter: true,
                sort: true,
                filterType: 'checkbox',
                filterOptions: {
                    names: ['TRAVIS', 'WILLIAMSON'],
                    logic(county, filters){
                        const show = 
                            (filters.indexOf('TRAVIS') >= 0 && county === 'TRAVIS') ||
                            (filters.indexOf('WILLIAMSON') >= 0 && county === 'WILLIAMSON');
                        return !show;

                    }
                },
                customBodyRender: (value, tableMeta, updateValue) =>
                    incidentCustomBodyRender(value, tableMeta, updateValue),
            },
        },
    ];

    const options = {
        filter: true,
        filterType: 'multiselect',
        onRowClick: (rowData) => {
          window.location.assign("/incidents/" + rowData[0]);
        },
        customSearch: (query, currentRow) => {
            let found = false;
            if (query.length < 2) {
                query = "";
            }
            setSearchText(query);
            currentRow.forEach((col) => {
              if (col.toString().toLowerCase().includes(query.toLowerCase())) {
                found = true;
              }
            });
            return found;
        },
        onSearchClose: () => {
            setSearchText("");
        },
        download: false,
        print: false,
        selectableRowsHideCheckboxes: true,
        selectableRowsHeader: false,
        viewColumns: false,
    };

    return (
        <div className="body">
            <div className="table-container">
                <div class="grid" style={{alignContent:'center'}}>
                    <div class="row align-items-center">
                        <div class="col-xl-4">
                            <img src={alert} style={{ width: "40%", height: "40%" }} alt="Unable to load"></img>
                        </div>
                        <div class="col-xl-4">
                            <h2 className="page-title">Incident Report</h2>
                            <p style={{textAlign:"center"}}>See nearby incidents happening around Austin.</p>
                            <h6 style={{textAlign:"center"}}>Click a column title to sort and click the icons in the top right to search and/or filter</h6>
                        </div>
                        <div class="col-xl-4">
                            <img src={alert} style={{ width: "40%", height: "40%" }} alt="Unable to load"></img>
                        </div>
                    </div>
                </div>
            </div>
            <div style={{display:'table', tableLayout:'fixed', width:'100%', height:'100%'}}>
                <MUIDataTable columns={columns} options={options} data={data}/>
            </div>
        </div>
    )
}

export default Incident;
