import React, {Component} from 'react';
import Leaflet from '../../Components/Leaflet'
import axios from 'axios';
import alert from "../Assets/alert.png"
import bed from "../Assets/bed.png"
import Card from "react-bootstrap/Card"
import 'leaflet/dist/leaflet.css';

const twoStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/2_stars.svg/1280px-2_stars.svg.png'
const twoHalfStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/2d/2.5_stars.svg/1280px-2.5_stars.svg.png'
const threeStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4e/3_stars.svg/1024px-3_stars.svg.png'
const threeHalfStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ee/3.5_stars.svg/1280px-3.5_stars.svg.png'
const fourStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/4_stars.svg/1200px-4_stars.svg.png'
const fourHalfStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/b/b9/4.5_stars.svg/1280px-4.5_stars.svg.png'
const fiveStars = 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/ae/5_stars.svg/1200px-5_stars.svg.png'

class RestaurantInstance extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            hotelData: null,
            restaurantData: null,
            incidentData: null
        }
    }

    async componentDidMount() {
        const url = window.location.href
        const idStartIndex = url.lastIndexOf('/') + 1
        const id = url.substring(idStartIndex)
        const restaurantRes = await axios.get(`/api/restaurants/id=${id}`);
        const hotelRes = await axios.get(`/api/hotels/zipcode=${restaurantRes.data['zipcode']}`);
        const incidentRes = await axios.get(`/api/incidents/zipcode=${restaurantRes.data['zipcode']}`);

        this.setState({
            restaurantData: restaurantRes.data,
            hotelData: hotelRes.data["hotels"],
            incidentData: incidentRes.data["incidents"]
        })
    }

    showStars(rating) {
        if (rating === 2.0)
            return twoStars;
        else if (rating === 2.5)
            return twoHalfStars;
        else if (rating === 3.0)
            return threeStars;
        else if (rating === 3.5)
            return threeHalfStars;
        else if (rating === 4.0)
            return fourStars;
        else if (rating === 4.5)
            return fourHalfStars;
        else 
            return fiveStars;
    }

    displayHotel() {
        if (this.state.hotelData == null) {
            return null;
        } else if (this.state.hotelData.length === 0) {
            return (<p>No hotels found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.hotelData.length; i++) {
            let hotel = this.state.hotelData[i];
            item.push (
                <React.Fragment>
                    <li>
                        <a href={"/hotels/" + hotel["hotel_id"]}>{hotel["name"]}</a>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    displayIncident() {
        if (this.state.incidentData == null) {
            return null;
        } else if (this.state.incidentData.length === 0) {
            return (<p>No incidents found.</p>)
        }
        var item = []
        for (var i = 0; i < this.state.incidentData.length; i++) {
            let incident = this.state.incidentData[i];
            var stop = incident['address'].indexOf(',');
            var street = this.capitalizeFirstLetters(incident['address'].substring(0, stop));

            item.push (
                <React.Fragment>
                    <li>
                        <a href={"/incidents/" + incident["incident_id"]}>{incident["name"]}</a>
                        <a>: {street}</a>
                    </li>
                </React.Fragment>
            );
        }
        return item.slice(0, 5);
    }

    capitalizeFirstLetters(text) {
        return text.toLowerCase().replace(/(^[a-z]| [a-z])/g, function (match, letter) {    
            return letter.toUpperCase();
        });
    }
    
    fixFulfillment() {
        let restaurantData = this.state.restaurantData;
        var str = restaurantData.fulfillment;
        if (str.length === 0) {
            return "NA";
        }
        var str1 = str[0].charAt(0).toUpperCase() + str[0].slice(1);
        if(str.length === 1) {
            return "Offers only " + str1;
        } else {
            var str2 = str[1].charAt(0).toUpperCase() + str[1].slice(1);
            return "Offers " + str1 + " and " + str2;
        }
    }

    render() {
        let hotelData = this.state.hotelData;
        let restaurantData = this.state.restaurantData;
        let incidentData = this.state.incidentData;
        
        return (hotelData != null && restaurantData != null && incidentData != null) ? (
            <React.Fragment>
                <div style={{margin: 'auto', marginTop: '10vh', width: '90%'}}>
                    <h1 className="text-center">{restaurantData.name}</h1>
                    <img src={this.showStars(restaurantData.rating)} style={{ width: "10%", height: "10%" }} alt="Unable to load"></img>
                    <br></br><br></br>
                    <h4 className="text-center">Scroll down to view more information!</h4>
                    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.0.1/dist/leaflet.css" />
                    <Leaflet latitude={restaurantData.latitude.toFixed(2)} longitude={restaurantData.longitude.toFixed(2)}></Leaflet>
                    <div className="row">
                        <div className="col-sm">
                            <Card.Body>
                                <Card.Title ><h2>Location</h2></Card.Title>
                                <Card.Text style={{fontSize:"25px"}}>
                                    {restaurantData.address}, {restaurantData.city}, {restaurantData.state} {restaurantData.zipcode}<br></br>
                                    {"Latitude: " + restaurantData.latitude}<br></br>
                                    {"Longitude: " + restaurantData.longitude}<br></br><br></br><br></br><br></br>
                                    <div class="grid" style={{alignContent:'center'}}>
                                        <div class="row align-items-center">
                                            <div class="col-xl-6">
                                                <Card.Img src={restaurantData.image} style={{width: "80%", height: "80%"}}/>
                                            </div>
                                            <div class="col-xl-6">
                                                <Card.Title><h2><br></br>Details</h2></Card.Title>
                                                {"Average Rating: " + restaurantData.rating}<br></br>
                                                {"Restaurant Type: " + restaurantData.type}<br></br>
                                                {"Review Count: " + restaurantData.review_count}<br></br>
                                                {"Price Level: " + restaurantData.price}<br></br>
                                                {"Phone: " + restaurantData.phone}<br></br>
                                                {"Fulfillment Options: " + this.fixFulfillment()}<br></br>
                                                {"Open?: " + restaurantData.is_open}<br></br>
                                                <a href={restaurantData.yelp_url}>Yelp Page</a><br></br><br></br><br></br><br></br>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="grid" style={{alignContent:'center'}}>
                                        <div class="row align-items-start">
                                            <div class="col-xl-6">
                                                <Card.Img variant="top" className="center" src={bed} style={{width: "15%"}}/>
                                                <Card.Title><h2><br></br>Nearby Hotels</h2></Card.Title>
                                                <div style={{listStyle:"none"}}>{this.displayHotel()}</div><br></br><br></br><br></br>
                                            </div>
                                            <div class="col-xl-6">
                                                <Card.Img variant="top" className="center" src={alert} style={{width: "15%"}}/>
                                                <Card.Title><h2><br></br>Nearby Incidents</h2></Card.Title>
                                                <div style={{listStyle:"none"}}>{this.displayIncident()}</div>
                                            </div>
                                        </div>
                                    </div>
                                </Card.Text>
                            </Card.Body>
                        </div> 
                    </div>
                </div>
            </React.Fragment>
        ) : (
            <div>
                <h2><br></br><br></br><br></br>Loading...</h2>
            </div>
        );
    }
}

export default RestaurantInstance;
