import {React, Component} from 'react'
import InfoCard from "./InfoCard"
import Jyoti from './ProfilePics/Jyoti.jpeg'
import Nikhil from './ProfilePics/Nikhil.jpeg'
import Jiaxi from './ProfilePics/Jiaxi.jpeg'
import Pranay from './ProfilePics/Pranay.jpeg'
import Tanmay from './ProfilePics/Tanmay.jpeg'
import GitLab from './ToolsPics/GitLabLogo.png'
import Postman from './ToolsPics/PostmanLogo.png'
import YouTube from './ToolsPics/YouTube.png'
import { toolsInfo, apiInfo } from './Resources'
import './About.css'

const COMMIT_URL = "https://gitlab.com/api/v4/projects/24679574/repository/commits?per_page=1000";
const ISSUE_URL = "https://gitlab.com/api/v4/projects/24679574/issues?per_page=1000";

class About extends Component {
	constructor(props) {
		super(props);
		this.state = {
			commits: null,
			issues: null,
			hasAllData: false
		}
	}

	async componentDidMount() {
		let commits = await this.fetchCommits();
		let issues = await this.fetchIssues();
		this.setState({commits: commits, issues: issues, hasAllData: true})
	}

	async fetchCommits() {
		let response = await fetch(COMMIT_URL);
		let commits = await response.json();
	
		return commits.length;
	}

	async fetchIssues() {
		let response = await fetch(ISSUE_URL);
		let json = await response.json();

		return json.length;
	}

	render() {
		return (
			<div className="body">
				<div className="intro">
					<h1 className="about-title">About Us</h1>
					<p>
						Plan your trip to the Live Music Capital of the world using Around ATX!
						This website will enable you to find information about hotels, restaurants 
						and incidents in the city so you can plan your stay safely. Our website links 
						information together to provide you with the best and safest experience when 
						you travel to Austin.
					</p>
				</div>
				<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" />
				<div className="grid">
					<div className="about-row">
						<InfoCard name={"Nikhil Kumar"} pic={Nikhil} />
						<InfoCard name={"Jyoti Luu"} pic={Jyoti} />
						<InfoCard name={"Pranay Kalagara"} pic={Pranay} />
					</div>
					<div className="about-row">
						<InfoCard name={"Tanmay Singh"} pic={Tanmay} />
						<InfoCard name={"Jiaxi Chen"} pic={Jiaxi} />
					</div>
				</div>
				<div className="intro">
					<h1>Repository Statistics</h1> <br></br>
					<div className="about-row">
						<div className="pad">
							<h4>Total Commits</h4>
							<div className="mt-3">
								<h4>{this.state.commits}</h4>
							</div>
						</div>
						<div>
							<div className="pad">
								<h4>Total Issues</h4>
								<div className="mt-3">
									<h4>{this.state.issues}</h4>
								</div>
							</div>
						</div>
						<div>
							<div className="pad">
								<h4>Total Tests</h4>
								<div className="mt-3">
									<h4>{50}</h4>
								</div>
							</div>
						</div>
					</div>
					<br></br><br></br>
					<h1>Project Links</h1>
					<div className="about-row">
						<div className="text-center card-box card-margin">
							<a href="https://gitlab.com/jyotiluu/cs373-aroundatx">
								<img src={GitLab} alt="Unable to load" className="medium" />
							</a>
							<br></br>
							<h3 className="text-center">GitLab Repo</h3>
						</div>
						<div className="text-center card-box card-margin">
							<a href="https://documenter.getpostman.com/view/14748761/Tz5jfLdN">
								<img src={Postman} alt="Unable to load" className="medium" />
							</a>
							<h3 className="text-center">API Collection</h3>
						</div>
						<div className="text-center card-box card-margin">
							<a href="https://youtu.be/rsXZf3EBLlc">
								<img src={YouTube} alt="Unable to load" className="medium" />
							</a>
							<h3 className="text-center">Presentation</h3>
						</div>
					</div>
					<br></br>
					<h1>Tools</h1>
					<ul className="p-0 pl-3" style={{ listStyleType: "none" }}>
						{toolsInfo.map((app, index) => (
						<li key={index} className="mb-2">
							<a className="link" href={app.link}>
							<img
								src={app.img}
								alt={app.title}
								style={{
								width: "2rem",
								marginRight: "1rem",
								}}
							></img>
							<strong>{app.title}</strong>
							</a>
							{app.description}
						</li>
						))}
					</ul>
					<br></br>
					<h1>RESTful APIs</h1>
					<ul className="p-0 pl-3" style={{ listStyleType: "none" }}>
						{apiInfo.map((app, index) => (
						<li key={index} className="mb-2">
							<a className="link" href={app.link}>
							<img
								src={app.img}
								alt={app.title}
								style={{
								width: "2rem",
								marginRight: "1rem",
								}}
							></img>
							<strong>{app.title}</strong>
							</a>
							{app.description}
						</li>
						))}
					</ul>
				</div>
			</div>
		);
	}
};

export default About;