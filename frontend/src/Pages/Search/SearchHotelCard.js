import {React, Component} from "react";
import {Highlight} from 'react-instantsearch-dom';
import { Card, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';
import '../Hotels/Hotels.css';

class SearchHotelCard extends Component {

	render() {
		let hit = this.props.hit;
		
		return (
			<Col>
				<Card className="mb-2" style={{height:"300px", width:"300px"}}>
					<Card.Img variant="top" src={hit.image} alt="profile-image" style={{height:"40%", width:"auto"}}/>
					<Card.Body>
						<Card.Title>
							<Link to={`/hotels/${hit.hotel_id}`}><div><Highlight hit={hit} attribute="name" /></div></Link>
						</Card.Title>
						<Card.Text>
							Star Rating: {hit.star_rating}/5 <br />
							Guest Rating: {hit.guest_rating}/10 <br />
							Distance to City Center: {hit.distance_to_city_center} miles <br />
							Distance to Airport: {hit.distance_to_airport} miles <br />
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		)
	}
}

export default SearchHotelCard;