import React, {Component} from "react";
import { Card, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';
import './Search.css';

class SearchIncidentCard extends Component {

	getHighlightedText(text, highlight) {
		let textLowerCases = text.toLowerCase();
		let hightLowerCases = highlight.toLowerCase();
		let matchedIndex = textLowerCases.indexOf(hightLowerCases);

		if (matchedIndex === -1)
			return text
		return (
			<React.Fragment>
				{text.substring(0, matchedIndex)}
				<span id="search-span">{text.substring(matchedIndex, matchedIndex + highlight.length)}</span>
				{text.substring(matchedIndex + highlight.length)}
			</React.Fragment>
		);
	}

	render() {
		let hit = this.props.hit;

		return (
			<Col>
				<Card className="mb-2" style={{height:"300px", width:"300px"}}>
					<Card.Body>
						<Card.Title>
							<Link to={`/incidents/${hit.incident_id}`}>
								<div>{this.getHighlightedText(hit.name, this.props.query)}</div>
								</Link>
						</Card.Title>
						<Card.Text>
							Department:{this.getHighlightedText(hit.department, this.props.query)} <br /><br />
							Created: {hit.created} <br /><br />
							Closed: {hit.closed} <br /><br />
							City: {this.getHighlightedText(hit.city, this.props.query)}<br /><br />
							Zipcode: {this.getHighlightedText(hit.zipcode, this.props.query)} <br /><br />
							Address: {this.getHighlightedText(hit.address, this.props.query)} <br /><br />
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		)
	}
}

export default SearchIncidentCard;