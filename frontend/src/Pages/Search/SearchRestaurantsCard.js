import React, {Component} from "react";
import { Card, Col } from "react-bootstrap";
import { Link } from 'react-router-dom';
import '../Hotels/Hotels.css';


class SearchRestaurantsCard extends Component {

	getHighlightedText(text, highlight) {
		text = String(text);
		let textLowerCases = text.toLowerCase();
		let hightLowerCases = highlight.toLowerCase();
		let matchedIndex = textLowerCases.indexOf(hightLowerCases);

		if (matchedIndex === -1)
			return text
		return (
			<React.Fragment>
				{text.substring(0, matchedIndex)}
				<span id="search-span">{text.substring(matchedIndex, matchedIndex + highlight.length)}</span>
				{text.substring(matchedIndex + highlight.length)}
			</React.Fragment>
		);
	}

	render() {
		let hit = this.props.hit;

		return (
			<Col>
				<Card className="mb-2" style={{height:"300px", width:"300px"}}>
					<Card.Img variant="top" src={hit.image} alt="profile-image" style={{height:"40%", width:"auto"}}/>
					<Card.Body>
						<Card.Title>
							<Link to={`/restaurants/${hit.restaurant_id}`}>
								<div>{this.getHighlightedText(hit.name, this.props.query)}</div>
							</Link>
						</Card.Title>
						<Card.Text>
							Type: {this.getHighlightedText(hit.type, this.props.query)} <br />
							Rating: {this.getHighlightedText(hit.rating, this.props.query)} <br />
							City: {this.getHighlightedText(hit.city, this.props.query)} <br />
							State: {this.getHighlightedText(hit.state, this.props.query)} <br />
							Zipcode: {this.getHighlightedText(hit.zipcode, this.props.query)} <br />
							Address: {this.getHighlightedText(hit.address, this.props.query)} <br />
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		)
	}
}

export default SearchRestaurantsCard;