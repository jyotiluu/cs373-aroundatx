import React from 'react';
import { Link } from "react-router-dom";
import { Nav, Navbar, Container } from 'react-bootstrap';
import logo from "./logo.png"
import "./Navbar.css";

function NavBar() {
    return (
        <div>
            {/* <Navbar collapseOnSelect expand="lg" variant="dark" style={{ background: "rgb(191, 87, 0)", height: "8vh"}} fixed="top"> */}
            <Navbar collapseOnSelect fixed='top' expand='lg' style={{background:'rgb(191, 87, 0)'}}>
                <Container>
                    <Navbar.Brand as={Link} to="/">
                        <img src={logo}  alt="Around ATX" width="50vh" height="50vh"/> Around ATX 
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls='responsive-navbar-nav' />
                    <Navbar.Collapse id='responsive-navbar-nav'>
                        <Nav style={{textAlign:"right"}}>
                            <Nav.Link href="/about">About</Nav.Link>
                            <Nav.Link href="/hotels">Hotels</Nav.Link>
                            <Nav.Link href="/restaurants">Restaurants</Nav.Link>
                            <Nav.Link href="/incidents">Incidents</Nav.Link>
                            <Nav.Link href="/visualizations">Our Visualizations</Nav.Link>
                            <Nav.Link href="/provider-visualizations">Provider Visualizations</Nav.Link>
                            <Nav.Link href="/search">Search</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    );
}

export default NavBar;