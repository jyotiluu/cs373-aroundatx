import React from "react";
import { MapContainer, Marker, TileLayer } from "react-leaflet";
import 'leaflet/dist/leaflet.css';

function Leaflet(props) {
    const position = [props.latitude, props.longitude]

    return (
        <React.Fragment>
            <div style={{margin: 'auto', marginTop: '15px', marginBottom: '15px', width: '100%'}}>
                <MapContainer center={position} zoom={13} scrollWheelZoom={true}
                    className="mx-auto d-block mb-5"
                    style={{width: "1000px", height: "500px", borderRadius: "25px 25px 25px 25px"}}
                    minZoom={4}>
                    <TileLayer
                        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"/>
                    <Marker position={position}></Marker>
                </MapContainer>
            </div>
        </React.Fragment>
    );
}

export default Leaflet;