import { render, screen } from '@testing-library/react';
import InfoCard from '../Pages/About/InfoCard.js'
import TestLogo from '../Pages/About/ToolsPics/GitLabLogo.png'

/** Tests the InfoCard using a test member.
	The inforCard should correctly displays its name, roles, bios */

// Tests the card shows the user name correctly
it('render test user InfoCard', () => {
	const userName = "TestUser12135";
	render(<div role="testDiv">
		<InfoCard name={userName} pic={TestLogo} />
	</div>)
	expect(screen.queryByText(userName)).toBeInTheDocument();
});

// Tests the card shows the role correstly
it('render test user InfoCard', () => {
	const roles = 'Full Stack Developer';
	render(<div role="testDiv">
		<InfoCard name={"TestUser12135"} pic={TestLogo} />
	</div>)
	expect(screen.queryByText(roles)).toBeInTheDocument();
});

// Tests the card shows the bio correstly
it('render test user InfoCard', () => {
	const bio = 'I am a Test Users. I love computers';
	render(<div role="testDiv">
		<InfoCard name={"TestUser12135"} pic={TestLogo} />
	</div>)
	expect(screen.queryByText(bio)).toBeInTheDocument();
});