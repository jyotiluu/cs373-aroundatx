import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from re import compile, M, Match, Pattern, search, split, sub
from datetime import datetime

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql+psycopg2://postgres:aroundatx@backend-db."
    "cvd9gpyejoue.us-east-2.rds.amazonaws.com:5432/postgres"
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Incidents(db.Model):
    incident_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    code = db.Column(db.String)
    department = db.Column(db.String)
    method_recieved = db.Column(db.String)
    description = db.Column(db.String)
    created = db.Column(db.String)
    updated = db.Column(db.String)
    closed = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    county = db.Column(db.String)
    council_district = db.Column(db.Integer)

    def __init__(
        self,
        incident_id,
        name,
        code,
        department,
        method_recieved,
        description,
        created,
        updated,
        closed,
        latitude,
        longitude,
        address,
        city,
        zipcode,
        county,
        council_district
    ):

        self.incident_id = incident_id
        self.name = name
        self.code = code
        self.department = department
        self.method_recieved = method_recieved
        self.description = description
        self.created = created
        self.updated = updated
        self.closed = closed
        self.latitude = latitude
        self.longitude = longitude
        self.address = address
        self.city = city
        self.zipcode = zipcode
        self.county = county
        self.council_district = council_district


db.create_all()
db.session.commit()

def parse_date(str):
    array = split("T",str)
    date = array[0]
    time = split("(.*:.*):", array[1])
    result = date + " " + time[1] 
    date = datetime.strptime(result, "%Y-%m-%d %H:%M")
    formatted_date = datetime.strftime(date, "%a %B %d, %Y %I:%M %p")
    return formatted_date

def insert_incidents():
    with open("data/incidents.json") as json_file:
        data = json.load(json_file)

    all_incidents = []

    for item in data:
        incident_id = item["sr_number"]
        name = item["sr_type_desc"]
        code = item["sr_type_code"]
        department = item["sr_department_desc"]
        method_recieved = item["sr_method_received_desc"]
        description = item["sr_status_desc"]
        created = item["sr_created_date"]
        created = parse_date(created)
        updated = item["sr_updated_date"]
        updated = parse_date(updated)

        if "sr_closed_date" not in item:
            closed = "Incident Still Open"
        else:
            closed = item["sr_closed_date"]
            closed = parse_date(closed)

        if "sr_location_lat_long" not in item:
            latitude = 0.00000
            longitude = 0.00000
        else:
            latitude = item["sr_location_lat_long"]["latitude"]
            longitude = item["sr_location_lat_long"]["longitude"]

        if "sr_location" not in item:
            address = "unknown"
        else:
            address = item["sr_location"]

        if "sr_location_city" not in item:
            city = "unknown"
        else:
            city = item["sr_location_city"]

        if "sr_location_zip_code" not in item:
            zipcode = 00000
        else:
            zipcode = item["sr_location_zip_code"]

        if isinstance(zipcode, str):
            zipcode = int(zipcode[:5])

        if "sr_location_county" not in item:
            county = "unknown"
        else:
            county = item["sr_location_county"]

        if "sr_location_council_district" not in item:
            council_district = 0
        else:
            council_district = int(item["sr_location_council_district"])

        incident = Incidents(
            incident_id,
            name,
            code,
            department,
            method_recieved,
            description,
            created,
            updated,
            closed,
            latitude,
            longitude,
            address,
            city,
            zipcode,
            county,
            council_district
        )

        all_incidents.append(incident)

    db.session.add_all(all_incidents)
    db.session.commit()


if __name__ == "__main__":
    insert_incidents()
