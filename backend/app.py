import json
import flask
from flask import Flask, render_template, request, jsonify
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from marshmallow import fields

app = Flask(
    __name__,
    static_folder="../frontend/build/static",
    template_folder="../frontend/build",
)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql+psycopg2://postgres:aroundatx@backend-db."
    "cvd9gpyejoue.us-east-2.rds.amazonaws.com:5432/postgres"
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["JSON_SORT_KEYS"] = False
app.debug = True

db = SQLAlchemy(app)
marsh = Marshmallow(app)
CORS(app)

###### MODELS ######
class Hotels(db.Model):
    hotel_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    star_rating = db.Column(db.Float)
    street_address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    state = db.Column(db.String)
    guest_rating = db.Column(db.Float)
    distance_to_city_center = db.Column(db.Float)
    distance_to_airport = db.Column(db.Float)
    neighborhood = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    image = db.Column(db.String)
    price = db.Column(db.Integer)
    rooms_left = db.Column(db.Integer)


class Restaurants(db.Model):
    restaurant_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    review_count = db.Column(db.Integer)
    type = db.Column(db.String)
    rating = db.Column(db.Float)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    fulfillment = db.Column(db.JSON)
    price = db.Column(db.String)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    state = db.Column(db.String)
    phone = db.Column(db.String)
    image = db.Column(db.String)
    yelp_url = db.Column(db.String)
    delivery = db.Column(db.String)
    is_open = db.Column(db.String)


class Incidents(db.Model):
    incident_id = db.Column(db.String, primary_key=True)
    name = db.Column(db.String)
    code = db.Column(db.String)
    department = db.Column(db.String)
    method_recieved = db.Column(db.String)
    description = db.Column(db.String)
    created = db.Column(db.String)
    updated = db.Column(db.String)
    closed = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    county = db.Column(db.String)
    council_district = db.Column(db.Integer)


###### SCHEMAS ######
class HotelSchema(marsh.Schema):
    class Meta:
        ordered = True

    hotel_id = fields.Integer(required=True)
    name = fields.String(required=False)
    star_rating = fields.Float(required=False)
    street_address = fields.String(required=False)
    city = fields.String(required=False)
    zipcode = fields.Integer(required=False)
    state = fields.String(required=False)
    guest_rating = fields.Float(required=False)
    distance_to_city_center = fields.Float(required=False)
    distance_to_airport = fields.Float(required=False)
    neighborhood = fields.String(required=False)
    latitude = fields.Float(required=False)
    longitude = fields.Float(required=False)
    image = fields.String(required=False)
    price = fields.Integer(required=False)
    rooms_left = fields.Integer(required=False)


class IncidentSchema(marsh.Schema):
    class Meta:
        ordered = True

    incident_id = fields.String(required=True)
    name = fields.String(required=False)
    code = fields.String(required=False)
    department = fields.String(required=False)
    method_recieved = fields.String(required=False)
    description = fields.String(required=False)
    created = fields.String(required=False)
    updated = fields.String(required=False)
    closed = fields.String(required=False)
    latitude = fields.Float(required=False)
    longitude = fields.Float(required=False)
    address = fields.String(required=False)
    city = fields.String(required=False)
    zipcode = fields.String(required=False)
    county = fields.String(required=False)
    council_district = fields.Integer(required=False)


class RestaurantSchema(marsh.Schema):
    class Meta:
        ordered = True

    restaurant_id = fields.String(required=True)
    name = fields.String(required=False)
    review_count = fields.Integer(required=False)
    type = fields.String(required=False)
    rating = fields.Float(required=False)
    latitude = fields.Float(required=False)
    longitude = fields.Float(required=False)
    fulfillment = fields.List(fields.String, required=False)
    price = fields.String(required=False)
    address = fields.String(required=False)
    city = fields.String(required=False)
    zipcode = fields.Integer(required=False)
    state = fields.String(required=False)
    phone = fields.String(required=False)
    image = fields.String(required=False)
    yelp_url = fields.String(required=False)
    delivery = fields.String(required=False)
    is_open = fields.String(required=False)

###### INITIALIZE SCHEMA OBJECTS ######

hotel_schema = HotelSchema()
hotels_schema = HotelSchema(many=True)

incident_schema = IncidentSchema()
incidents_schema = IncidentSchema(many=True)

restaurant_schema = RestaurantSchema()
restaurants_schema = RestaurantSchema(many=True)

### API ENDPOINTS ###

# Routing to serve the frontend
@app.route("/", defaults={"path": ""})
@app.route("/<path:path>")
def index(path):
    return render_template("index.html")


# Retrieve all hotels
@app.route("/api/hotels", methods=["GET"])
def get_hotels():

    # Initial query
    all_hotels = Hotels.query

    # Evaluates sorting parameters
    sort_by = str(request.args.get('sort_by'))
    sort_direction = str(request.args.get('sort_direction'))

    # Evaluate filtering parameters and set to None if not provided
    name_from = str(request.args.get('name_from'))
    name_to = str(request.args.get('name_to'))

    try:
        star_rating_from = int(request.args.get('star_rating_from'))
        star_rating_to = int(request.args.get('star_rating_to'))
    except:
        star_rating_from = None
        star_rating_to = None

    try:
        guest_rating_from = int(request.args.get('guest_rating_from'))
        guest_rating_to = int(request.args.get('guest_rating_to'))
    except:
        guest_rating_from = None
        guest_rating_to = None

    try:
        distance_to_city_center_from = int(request.args.get('distance_to_city_center_from'))
        distance_to_city_center_to = int(request.args.get('distance_to_city_center_to'))
    except:
        distance_to_city_center_from = None
        distance_to_city_center_to = None

    try:
        distance_to_airport_from = int(request.args.get('distance_to_airport_from'))
        distance_to_airport_to = int(request.args.get('distance_to_airport_to'))
    except:
        distance_to_airport_from = None
        distance_to_airport_to = None

    # Allows lookup of sortable and filterable attributes
    attributes = {
        "name": Hotels.name,
        "star_rating": Hotels.star_rating,
        "guest_rating": Hotels.guest_rating,
        "distance_to_city_center": Hotels.distance_to_city_center,
        "distance_to_airport": Hotels.distance_to_airport,
    }

    # Flag indicating a default response should be return if no parameters are supplied
    default = True

    # Sort and filter as needed based on the parameters supplied
    if sort_direction is not None and sort_by is not None:
        if sort_direction == 'asc':
            all_hotels = all_hotels.order_by(attributes.get(sort_by).asc())
        elif sort_direction == 'desc':
            all_hotels = all_hotels.order_by(attributes.get(sort_by).desc())
        
        default = False

    if name_from != 'None' and name_to != 'None':
        try:
            all_hotels = all_hotels.filter(attributes['name'] >= name_from)
            all_hotels = all_hotels.filter(attributes['name'] <= name_to)
        except:
            pass

        default = False

    if star_rating_from is not None and star_rating_to is not None:
        try:
            all_hotels = all_hotels.filter(attributes['star_rating'] >= star_rating_from)
            all_hotels = all_hotels.filter(attributes['star_rating'] <= star_rating_to)
        except:
            pass

        default = False
    
    if guest_rating_from is not None and guest_rating_to is not None:
        try:
            all_hotels = all_hotels.filter(attributes['guest_rating'] >= guest_rating_from)
            all_hotels = all_hotels.filter(attributes['guest_rating'] <= guest_rating_to)
        except:
            pass
        
        default = False

    if distance_to_city_center_from is not None and distance_to_city_center_to is not None:
        try:
            all_hotels = all_hotels.filter(attributes['distance_to_city_center'] >= distance_to_city_center_from)
            all_hotels = all_hotels.filter(attributes['distance_to_city_center'] <= distance_to_city_center_to)
        except:
            pass
        
        default = False
    
    if distance_to_airport_from is not None and distance_to_airport_to is not None:
        try:
            all_hotels = all_hotels.filter(attributes['distance_to_airport'] >= distance_to_airport_from)
            all_hotels = all_hotels.filter(attributes['distance_to_airport'] <= distance_to_airport_to)
        except:
            pass
            
        default = False

    if default:
        all_hotels = all_hotels.all()

    # Dump the resulting query into our schema object
    result = hotels_schema.dump(all_hotels,)

    # Return a prettified json representation of the data
    return jsonify({"hotels": result})


# Retrieve a single hotel by id
@app.route("/api/hotels/id=<id>", methods=["GET"])
def get_hotel_by_id(id):
    # Query into database
    hotel = Hotels.query.get(id)
    # If id does not exist, return an error 404
    if hotel is None:
        response = flask.Response(
            json.dumps({"error": id + " is not a valid hotel id"}),
            mimetype="application/json",
        )
        response.status_code = 404
        return response
    return hotel_schema.jsonify(hotel)


# Retrieve hotels with a particular zipcode
@app.route("/api/hotels/zipcode=<zipcode>", methods=["GET"])
def get_hotel_by_zipcode(zipcode):

    # Error check for a valid zipcode
    try:
        zipcode = int(zipcode)
    except:
        response = flask.Response(
            json.dumps({"error": "invalid zipcode"}), mimetype="application/json"
        )
        response.status_code = 404
        return response

    # Query into database
    all_hotels = Hotels.query.all()
    response = hotels_schema.dump(all_hotels)
    result = []

    for item in response:
        if item["zipcode"] == zipcode:
            result.append(item)

    return jsonify({"hotels": result})


# Retrieve all restaurants
@app.route("/api/restaurants", methods=["GET"])
def get_restaurants():
    # Query into database
    all_restaurants = Restaurants.query.all()
    result = restaurants_schema.dump(all_restaurants)
    return jsonify({"restaurants": result})


# Retrieve a single restaurant by name
@app.route("/api/restaurants/id=<id>", methods=["GET"])
def get_restaurant_by_id(id):
    # Query into database
    restaurant = Restaurants.query.get(id)
    # If id does not exist, return an error 404
    if restaurant is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return restaurant_schema.jsonify(restaurant)


# Retrieve restaurants with a particular zipcode
@app.route("/api/restaurants/zipcode=<zipcode>", methods=["GET"])
def get_restaurant_by_zipcode(zipcode):

    # Error check for a valid zipcode
    try:
        zipcode = int(zipcode)
    except:
        response = flask.Response(
            json.dumps({"error": "invalid zipcode"}), mimetype="application/json"
        )
        response.status_code = 404
        return response

    # Query into database
    all_restaurants = Restaurants.query.all()
    response = restaurants_schema.dump(all_restaurants)
    result = []

    for item in response:
        if item["zipcode"] == zipcode:
            result.append(item)

    return jsonify({"restaurants": result})


# Retrieve all incidents
@app.route("/api/incidents", methods=["GET"])
def get_incidents():
    # Query into database
    all_incidents = Incidents.query.all()
    result = incidents_schema.dump(all_incidents)
    return jsonify({"incidents": result})


# Retrieve a single incident by name
@app.route("/api/incidents/id=<id>", methods=["GET"])
def get_incident_by_id(id):
    # Query into database
    incident = Incidents.query.get(id)
    # If id does not exist, return an error 404
    if incident is None:
        response = flask.Response(
            json.dumps({"error": id + " not found"}), mimetype="application/json"
        )
        response.status_code = 404
        return response
    return incident_schema.jsonify(incident)


# Retrieve incidents with a particular zipcode
@app.route("/api/incidents/zipcode=<zipcode>", methods=["GET"])
def get_incident_by_zipcode(zipcode):

    # Error check for a valid zipcode
    try:
        int(zipcode)
    except:
        response = flask.Response(
            json.dumps({"error": "invalid zipcode"}), mimetype="application/json"
        )
        response.status_code = 404
        return response

    # Query into database
    all_incidents = Incidents.query.all()
    response = incidents_schema.dump(all_incidents)
    result = []

    for item in response:
        if item["zipcode"] == zipcode:
            result.append(item)

    return jsonify({"incidents": result})


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=80, threaded=True, debug=True)
