import requests
import json
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    "postgresql+psycopg2://postgres:aroundatx@backend-db."
    "cvd9gpyejoue.us-east-2.rds.amazonaws.com:5432/postgres"
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Hotels(db.Model):
    hotel_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    star_rating = db.Column(db.Float)
    street_address = db.Column(db.String)
    city = db.Column(db.String)
    zipcode = db.Column(db.Integer)
    state = db.Column(db.String)
    guest_rating = db.Column(db.Float)
    distance_to_city_center = db.Column(db.Float)
    distance_to_airport = db.Column(db.Float)
    neighborhood = db.Column(db.String)
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    image = db.Column(db.String)
    price = db.Column(db.Integer)
    rooms_left = db.Column(db.Integer)

    def __init__(
        self,
        hotel_id,
        name,
        star_rating,
        street_address,
        city,
        zipcode,
        state,
        guest_rating,
        distance_to_city_center,
        distance_to_airport,
        neighborhood,
        latitude,
        longitude,
        image,
        price, 
        rooms_left
    ):
        self.hotel_id = hotel_id
        self.name = name
        self.star_rating = star_rating
        self.street_address = street_address
        self.city = city
        self.zipcode = zipcode
        self.state = state
        self.guest_rating = guest_rating
        self.distance_to_city_center = distance_to_city_center
        self.distance_to_airport = distance_to_airport
        self.neighborhood = neighborhood
        self.latitude = latitude
        self.longitude = longitude
        self.image = image
        self.price = price
        self.rooms_left = rooms_left


db.create_all()
db.session.commit()


def insert_hotels():
    with open("data/hotels.json") as json_file:
        data = json.load(json_file)

    all_hotels = []

    for item in data["data"]["body"]["searchResults"]["results"]:
        hotel_id = item["id"]
        name = item["name"]
        star_rating = item["starRating"]
        street_address = item["address"]["streetAddress"]
        city = item["address"]["locality"]
        zipcode = item["address"]["postalCode"]

        if isinstance(zipcode, str):
            zipcode = int(zipcode[:5])
            
        state = item["address"]["region"]
        guest_rating = item["guestReviews"]["unformattedRating"]
        distance_to_city_center = float(item["landmarks"][0]["distance"].split()[0])
        distance_to_airport = float(item["landmarks"][1]["distance"].split()[0])
        neighborhood = item["neighbourhood"]
        latitude = item["coordinate"]["lat"]
        longitude = item["coordinate"]["lon"]
        image = item["optimizedThumbUrls"]["srpDesktop"]
        price = int((item["ratePlan"]["price"]["current"])[1:])

        if 'roomsLeft' not in item:
            rooms_left = 0
        else:
            rooms_left = item["roomsLeft"]

        hotel = Hotels(
            hotel_id,
            name,
            star_rating,
            street_address,
            city,
            zipcode,
            state,
            guest_rating,
            distance_to_city_center,
            distance_to_airport,
            neighborhood,
            latitude,
            longitude,
            image,
            price, 
            rooms_left
        )

        all_hotels.append(hotel)

    db.session.add_all(all_hotels)
    db.session.commit()


if __name__ == "__main__":
    insert_hotels()
